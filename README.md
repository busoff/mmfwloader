# MindMotion Firmware Loader
This project implements a firmware loader for MindMotion MCUs using UART communication. Currently, it only supports the MM32F0020 chip, but it can be extended to other chips in the future. It also includes a flash memory dump feature.

## Build
```
git clone git@gitlab.com:busoff/mmfwloader.git
cd mmfwloader
cargo build
cargo run -- --help
```

## Usage
- Dump flash memory
```
mmfwloader /dev/ttyUSB0 mem -n 256 --start 0x08000000 -o flash.bin
```

- Flashing firmware only support hex file
```
mmfwloader /dev/ttyUSB0 fw firmware.hex
```

