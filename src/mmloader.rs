use core::fmt;
use std::str::FromStr;

use anyhow::{bail, Context, Result};
use indicatif::{ProgressBar, ProgressStyle};
use serialport::SerialPort;

#[derive(Clone)]
pub struct FW {
    pub load_address: u32,
    pub data: Vec<u8>,
    pub checksum: u32,
}

#[derive(Clone, Debug, PartialEq, Copy)]
pub enum Chip {
    MM32F0020,
    MM32F0140,
}

impl fmt::Display for Chip {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Chip::MM32F0020 => write!(f, "MM32F0020"),
            Chip::MM32F0140 => write!(f, "MM32F0140"),
        }
    }
}

impl Into<String> for Chip {
    fn into(self) -> String {
        match self {
            Chip::MM32F0020 => "MM32F0020".to_string(),
            Chip::MM32F0140 => "MM32F0140".to_string(),
        }
    }
}

impl FromStr for Chip {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "mm32f0020" => Ok(Chip::MM32F0020),
            "mm32f0140" => Ok(Chip::MM32F0140),
            // Add other variants as needed
            _ => Err(format!("'{}' is not a valid chip type", s)),
        }
    }
}

#[repr(u8)]
pub enum ClearMode {
    ClearChip = 0,
    ClearSectors = 1,
    ClearNone = 2,
}

pub struct MMBootLoader {
    serial_port: Box<dyn SerialPort>,
    baudrate: u32,
    ram_bin: Vec<u8>,
    ram_load_start: u32,
    verbose: bool,
}

impl FW {
    // a function load a intel hex file and return a FW struct
    pub fn load_from_ihex(filename: &str) -> Result<Self> {
        let contents =
            std::fs::read_to_string(filename).with_context(|| "Failed to open FW file")?;
        let reader = ihex::Reader::new(&contents);
        let mut bin: Vec<u8> = Vec::new();
        let mut load_address = 0u32;
        for record in reader {
            let record = record.unwrap();
            match record {
                ihex::Record::Data {
                    offset: _,
                    mut value,
                } => {
                    bin.append(&mut value);
                }
                ihex::Record::ExtendedLinearAddress(base) => {
                    load_address = (base as u32) << 16;
                }
                _ => (),
            }
        }

        Ok(Self {
            load_address,
            checksum: Self::checksum(&bin),
            data: bin,
        })
    }

    /// Calculate checksum of a byte slice by adding all u16 values together
    fn checksum(data: &[u8]) -> u32 {
        let mut checksum = 0u64;
        let mut i = 0;
        while i < data.len() {
            checksum += data[i] as u64 | (data[i + 1] as u64) << 8;
            i += 2;
        }
        checksum as u32
    }
}

impl MMBootLoader {
    fn load_ram_hex_file(chip: &Chip) -> Result<(Vec<u8>, u32)> {
        let file_content = match chip {
            Chip::MM32F0020 => include_str!("../rambin/MM32F0020.hex"),
            Chip::MM32F0140 => include_str!("../rambin/MM32F0140.hex"),
        };
        let reader = ihex::Reader::new(&file_content);
        let mut bin: Vec<u8> = Vec::new();
        let mut start_address = 0x20000000u32;
        let mut start_offset = None;
        for record in reader {
            let record = record.unwrap();
            match record {
                ihex::Record::Data { offset, mut value } => {
                    if start_offset.is_none() {
                        start_offset = Some(offset);
                    }
                    bin.append(&mut value);
                }
                ihex::Record::ExtendedLinearAddress(base) => {
                    start_address = (base as u32) << 16;
                }
                _ => (),
            }
        }
        Ok((bin, start_address + start_offset.unwrap() as u32))
    }

    pub fn new(port: &str, baudrate: u32, chip: &Chip, verbose: bool) -> Result<Self> {
        let port = serialport::new(port, 9600)
            .timeout(std::time::Duration::from_millis(1000))
            .open()?;
        let (ram_bin, ram_load) = Self::load_ram_hex_file(&chip)?;
        let s: Self = Self {
            serial_port: port,
            baudrate: baudrate,
            ram_bin,
            ram_load_start: ram_load,
            verbose,
        };

        Ok(s)
    }

    pub fn initialize(&mut self) -> Result<()> {
        self.handshake()?;

        match self.set_baudrate(true) {
            Err(_) => {
                // possible in L2 bootloader?  skip ran_code_info() & send_ram_code()
                self.set_baudrate(false)?;
            }
            Ok(_) => {
                self.ram_code_info()?;
                self.send_ram_code()?;
                self.set_baudrate(false)?;
            }
        }
        Ok(())
    }

    pub fn read_memory_block(&mut self, start: usize, num: usize) -> Result<Vec<u8>> {
        let total_u32 = (num + 3) / 4;
        let mut buffer = vec![0; total_u32];
        let pb = ProgressBar::new(num as u64);
        pb.set_style(ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({eta})")
        .unwrap()
        .progress_chars("#>-"));

        for i in 0..total_u32 {
            buffer[i] = self.read_memory_u32(start + i * 4)?;
            pb.set_position(((i + 1) * 4) as u64);
        }
        pb.finish_with_message("done");

        let bytes: Vec<u8> = buffer
            .iter()
            .flat_map(|&x| x.to_be_bytes().to_vec())
            .collect();
        Ok(bytes[..num].to_vec())
    }

    fn checksum(bytes: &[u8]) -> u8 {
        let sum: u32 = bytes.iter().map(|b| *b as u32).sum();
        sum as u8
    }

    pub fn handshake(&mut self) -> Result<()> {
        let mut handshake = vec![0x50, 0x00, 0x05, 0x00];
        handshake.push(Self::checksum(handshake.as_slice()));
        for baudrate in Self::POSSIBLE_BAUDRATES {
            self.serial_port.set_baud_rate(baudrate)?;
            match self.send_receive(handshake.as_slice()) {
                Ok(frame) => {
                    if frame != vec![0x53, 0x00, 0x05, 0x00, 0x58] {
                        bail!("Received handshake response invalid");
                    } else {
                        return Ok(());
                    }
                }
                Err(error) => {
                    if let Some(io_error) = error.downcast_ref::<std::io::Error>() {
                        if io_error.kind() == std::io::ErrorKind::TimedOut {
                            continue;
                        } else {
                            return Err(error);
                        }
                    } else {
                    }
                }
            }
        }
        bail!("Handshake failed");
    }

    fn send_receive(&mut self, tx_buf: &[u8]) -> Result<Vec<u8>> {
        self.serial_port.write_all(tx_buf)?;
        if self.verbose {
            eprintln!(
                "-> {}",
                tx_buf
                    .iter()
                    .map(|x| format!("{:02x}", x))
                    .collect::<Vec<_>>()
                    .join(" ")
            );
        }
        let frame = self.receive_frame()?;
        if self.verbose {
            eprintln!(
                "<- {}",
                frame
                    .iter()
                    .map(|x| format!("{:02x}", x))
                    .collect::<Vec<_>>()
                    .join(" ")
            );
        }
        Ok(frame)
    }

    pub fn set_baudrate(&mut self, l1_bootloader: bool) -> Result<()> {
        let baud: u32 = self.baudrate / 2400;
        let mut tx_buf: Vec<u8> = vec![0x50, 0x00, 0x07, 0x20, 0x03, baud as u8];
        tx_buf.push(Self::checksum(tx_buf.as_slice()));

        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0x20 {
            bail!("Received invalid set_baudrate response");
        }

        if l1_bootloader {
            if frame[4] != 0x56 {
                bail!("Received invalid set_baudrate response in L1 bootloader");
            }
        } else {
            if frame[4] != 0x53 {
                bail!("Received invalid set_baudrate response in L2 bootloader");
            }
        }

        if frame[8] != 0x03 {
            bail!("Received invalid set_baudrate response");
        }

        let baudrate: u32 = frame[9] as u32 * 2400;
        if baudrate != 0 && baudrate != self.baudrate {
            bail!("Set baudrate failed for L1 bootloader");
        }

        // reconfigure serial port with new baudrate
        self.serial_port.set_baud_rate(self.baudrate)?;
        Ok(())
    }

    pub fn ram_code_info(&mut self) -> Result<()> {
        println!("ram_bin size: {}", self.ram_bin.len());
        println!("ram_load address: {:x}", self.ram_load_start);
        let mut tx_buf = vec![
            0x50, 0x00, 0x11, 0x02, // 0x02 Configure upload address
            0x00, 0x00, 0x00, 0x00, // reserved
        ];
        tx_buf.push((self.ram_load_start >> 24) as u8);
        tx_buf.push((self.ram_load_start >> 16) as u8);
        tx_buf.push((self.ram_load_start >> 8) as u8);
        tx_buf.push(self.ram_load_start as u8);

        let size = self.ram_bin.len();
        tx_buf.push((size >> 24) as u8);
        tx_buf.push((size >> 16) as u8);
        tx_buf.push((size >> 8) as u8);
        tx_buf.push(size as u8);
        tx_buf.push(Self::checksum(tx_buf.as_slice()));

        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0x02 {
            bail!("invalid ram code info response received");
        }
        Ok(())
    }

    pub fn send_ram_code(&mut self) -> Result<()> {
        let blocks = (self.ram_bin.len() + 255) / 256;
        for i in 0..blocks - 1 {
            self.send_ram_code_block(i, false)?;
        }
        self.send_ram_code_block(blocks - 1, true)?;
        Ok(())
    }

    fn send_ram_code_block(&mut self, num: usize, last_block: bool) -> Result<()> {
        let last_block: u8 = if last_block { 2 } else { 1 };
        let load_addr = self.ram_load_start + num as u32 * 256;
        let mut tx_buf = vec![
            0x50, 0x01, // Length of this frame to be sent out
            0x0D, 0x02, // 0x01 Setup Flash Burn of Hexfile
            0x00, // Frame number [1-6]
            0x00, 0x00, last_block, // All frames get 1, except the final frame which gets 2
        ];
        // fill the load address
        load_addr.to_be_bytes().iter().for_each(|b| tx_buf.push(*b));

        if (num + 1) * 256 > self.ram_bin.len() {
            tx_buf.extend_from_slice(&self.ram_bin[num * 256..])
        } else {
            tx_buf.extend_from_slice(&self.ram_bin[num * 256..(num + 1) * 256])
        };
        tx_buf.push(Self::checksum(tx_buf.as_slice()));

        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0x02 {
            bail!("invalid downloading ram code response received");
        }

        Ok(())
    }

    fn read_memory_u32(&mut self, address: usize) -> Result<u32> {
        let mut tx_buffer = vec![
            0x50, 0x00, 0x09, 0x06, // Read a 32-bit word from device memory
        ];
        tx_buffer.push((address >> 24) as u8);
        tx_buffer.push((address >> 16) as u8);
        tx_buffer.push((address >> 8) as u8);
        tx_buffer.push((address >> 0) as u8);
        tx_buffer.push(Self::checksum(tx_buffer.as_slice()));
        let frame = self.send_receive(&tx_buffer)?;

        if frame[3] != 0x06 {
            bail!("invalid read memory u32 response received");
        }

        let recv_addr = (frame[4] as usize) << 24
            | (frame[5] as usize) << 16
            | (frame[6] as usize) << 8
            | (frame[7] as usize);

        if recv_addr != address {
            bail!("invalid read memory u32 response received");
        }
        let recv_value: u32 = (frame[11] as u32) << 24
            | (frame[10] as u32) << 16
            | (frame[9] as u32) << 8
            | (frame[8] as u32);
        Ok(recv_value)
    }

    pub fn write_memory_u32(&mut self, address: usize, value: u32) -> Result<()> {
        let mut tx_buffer = vec![
            0x50, 0x00, 0x0D, 0x05, // Read a 32-bit word from device memory
        ];
        tx_buffer.push((address >> 24) as u8);
        tx_buffer.push((address >> 16) as u8);
        tx_buffer.push((address >> 8) as u8);
        tx_buffer.push((address >> 0) as u8);
        value
            .to_be()
            .to_be_bytes()
            .iter()
            .for_each(|b| tx_buffer.push(*b));
        tx_buffer.push(Self::checksum(tx_buffer.as_slice()));
        eprintln!(
            "<- {}",
            tx_buffer
                .iter()
                .map(|x| format!("{:02x}", x))
                .collect::<Vec<_>>()
                .join(" ")
        );
        let frame = self.send_receive(&tx_buffer)?;

        if frame[3] != 0x05 && frame[0] != 0x53 {
            bail!("invalid write memory u32 response received");
        }

        let recv_addr = (frame[4] as usize) << 24
            | (frame[5] as usize) << 16
            | (frame[6] as usize) << 8
            | (frame[7] as usize);

        if recv_addr != address {
            bail!("invalid write memory u32 response received");
        }

        if self.read_memory_u32(address)? != value {
            bail!("write memory u32 failed");
        }
        Ok(())
    }

    pub fn download_fw<F>(&mut self, fw: &FW, mode: ClearMode, update: F) -> Result<()>
    where
        F: FnMut(f32, f32) -> (),
    {
        self.send_fw_info(&fw, mode)?;
        self.load_fw(&fw, update)?;
        self.verify_fw(&fw)?;
        Ok(())
    }

    pub fn send_fw_info(&mut self, fw: &FW, mode: ClearMode) -> Result<()> {
        let size = fw.data.len();
        let mut tx_buf = vec![
            0x50,
            0x00,
            0x12,
            0x01, // 0x01 Setup Flash Burn of Hexfile
            0x00,
            0x00,
            0x00,
            0x00,
            (fw.load_address >> 24) as u8,
            (fw.load_address >> 16) as u8,
            (fw.load_address >> 8) as u8,
            (fw.load_address) as u8,
            (size >> 24) as u8,
            (size >> 16) as u8,
            (size >> 8) as u8,
            (size) as u8,
            mode as u8,
        ];

        tx_buf.push(Self::checksum(&tx_buf));

        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0x01 {
            bail!("invalid fw info response received");
        }
        Ok(())
    }

    pub fn load_fw<F>(&mut self, fw: &FW, mut update: F) -> Result<()>
    where
        F: FnMut(f32, f32) -> (),
    {
        let binary = fw.data.to_vec();
        let num_of_blocks = (binary.len() + 255) / 256;

        for i in 0..num_of_blocks - 1 {
            self.send_fw_block(&binary[i * 256..(i + 1) * 256], i + 1, num_of_blocks)?;
            // pb.set_position(((i + 1) * 256) as u64);
            update(((i + 1) * 256) as f32, binary.len() as f32);
        }
        self.send_fw_block(
            &binary[(num_of_blocks - 1) * 256..],
            num_of_blocks,
            num_of_blocks,
        )?;
        update(binary.len() as f32, binary.len() as f32);

        Ok(())
    }

    pub fn verify_fw(&mut self, fw: &FW) -> Result<()> {
        let mut tx_buf = vec![0x50, 0x00, 0x05, 0x0F];
        tx_buf.push(Self::checksum(&tx_buf));
        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0x0F {
            bail!("Received unexpected response");
        }
        let received_checksum = (frame[4] as u32) << 24
            | (frame[5] as u32) << 16
            | (frame[6] as u32) << 8
            | (frame[7] as u32);
        if received_checksum != fw.checksum {
            bail!(format!(
                "Checksum mismatch (actual {:x}, expect {:x})",
                received_checksum, fw.checksum
            ));
        }

        Ok(())
    }

    fn send_fw_block(&mut self, block: &[u8], block_num: usize, total_blocks: usize) -> Result<()> {
        let mut tx_buf = vec![
            0x50u8,
            0x01,
            0x0D,
            0x01, // 0x01 Write data block
            (total_blocks >> 24) as u8,
            (total_blocks >> 16) as u8,
            (total_blocks >> 8) as u8,
            (total_blocks >> 0) as u8,
            (block_num >> 24) as u8,
            (block_num >> 16) as u8,
            (block_num >> 8) as u8,
            (block_num >> 0) as u8,
        ];
        tx_buf.append(&mut block.to_vec());
        if block.len() < 256 {
            // fill 0xff to reach 256 bytes
            for _i in block.len()..256 {
                tx_buf.push(0xff);
            }
        }

        tx_buf.push(Self::checksum(&tx_buf));
        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0x01 {
            bail!("Received unexpected response");
        }

        Ok(())
    }

    pub fn reset(&mut self) -> Result<()> {
        let mut tx_buf = vec![
            0x50, 0x00, 0x05, 0xFF, // 0xFF  Reset MindMotion CPU
        ];
        tx_buf.push(Self::checksum(&tx_buf));
        let frame = self.send_receive(&tx_buf)?;
        if frame[3] != 0xFF {
            bail!("Received unexpected response");
        }

        Ok(())
    }

    fn receive_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        let mut reads = 0 as usize;
        while reads < buf.len() {
            let n = self.serial_port.read(&mut buf[reads..])?;
            reads += n;
        }
        Ok(())
    }

    fn receive_frame(&mut self) -> Result<Vec<u8>> {
        let mut buffer = [0u8; 128];
        self.receive_exact(&mut buffer[0..3])?;
        if buffer[0] != 0x53 {
            bail!("Received unexpected header");
        }
        let size: usize = (buffer[1] as usize) << 8 | buffer[2] as usize;
        if size > 128 {
            bail!("Too large size");
        } else if size < 3 {
            bail!("size less than header size");
        }
        self.receive_exact(&mut buffer[3..size])?;
        if buffer[size - 1] != Self::checksum(&buffer[0..size - 1]) {
            bail!("checksum error in received");
        }
        Ok(Vec::from(&buffer[0..size]))
    }

    const POSSIBLE_BAUDRATES: [u32; 4] = [9600, 115200, 460800, 600000];
}
