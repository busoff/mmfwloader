use anyhow::{bail, Result};
use clap::{ArgAction, Parser, Subcommand};
use clap_num::maybe_hex;
use console::style;
use hexdump::hexdump;
use indicatif::{ProgressBar, ProgressStyle};
use serialport;
use std::fs::File;
use std::io::Write;
use std::str::FromStr;
use std::thread;
use std::time::Duration;

use mmloader::{Chip, ClearMode, FW};
use mmloader::{Device, LEDDriver, LightEngine};

fn write_to_file(filename: &str, buffer: &[u8]) -> Result<()> {
    let mut file = File::create(filename)?;
    file.write_all(buffer)?;
    Ok(())
}

#[derive(Parser, Debug)]
struct Cli {
    pub port: String,
    #[arg(short, long, value_parser= clap::builder::ValueParser::new(Chip::from_str), default_value = "mm32f0020")]
    pub chip: Chip,
    #[command(subcommand)]
    pub command: Cmd,
}
#[derive(Clone, Debug, Subcommand)]
pub enum BootStep {
    /// enter bootloader
    Bl,
    /// send handshake
    Hs,
    /// set baudrate for L1 bootloader
    BrL1,
    /// set baudrate for L2 bootloader
    BrL2,
    /// Set RAM code info
    RamCodeInfo,
    /// RAM code
    RamCode,
    /// Send burn in firmware info
    PmInfo { firmware: String },
    /// Burn in firmware
    Pm { firmware: String },
    /// Verify firmware
    Vf { firmware: String },
    /// Reset MCU
    Reset,
}

#[derive(Parser, Debug, Clone)]
struct MemCmd {
    #[arg(short, long, value_parser=maybe_hex::<u32>, default_value = "0x80000000")]
    start_address: u32,
    #[arg(short, long, value_parser=maybe_hex::<u32>, default_value = "256")]
    num: u32,
    #[arg(short, long)]
    output_file: Option<String>,
    #[arg(short, long)]
    compare: Option<String>,
    #[arg(short, long, value_parser=maybe_hex::<u32>)]
    write: Option<u32>,
}

#[derive(Subcommand, Debug, Clone)]
enum Cmd {
    Mem(MemCmd),
    Fw {
        firmware: String,
        #[arg(short, long, action = ArgAction::SetFalse)]
        clear_chip: bool,
    },
    Bs {
        #[command(subcommand)]
        bootstep: BootStep,
    },
}

fn main() -> Result<()> {
    let matches = Cli::parse();

    let port = matches.port;
    let mut dev: Box<dyn Device> = match matches.chip {
        Chip::MM32F0020 => Box::new(LEDDriver::new(&port, Chip::MM32F0020)?),
        Chip::MM32F0140 => Box::new(LightEngine::new(&port, Chip::MM32F0140)?),
    };

    match matches.command {
        Cmd::Fw {
            firmware,
            clear_chip,
        } => {
            loop {
                let result = handle_fw_subcmd(&mut *dev, &firmware, clear_chip);
                if result.is_ok() {
                    break;
                } else {
                    let err = result.unwrap_err();
                    if let Some(e) = err.downcast_ref::<serialport::Error>() {
                        if e.description == "Access is denied." {
                            eprintln!("Abort due to {} ", e);
                            break;
                        }
                    }
                    eprintln!("Error: {} and retry after 1s", err);
                    thread::sleep(Duration::from_millis(1000)); // Wait for 1 second
                }
            }
        }
        Cmd::Mem(cmd) => {
            if let Err(err) = handle_mem_subcmd(&mut *dev, cmd) {
                eprintln!("Error: {}", err);
            }
        }
        Cmd::Bs { bootstep } => match bootstep {
            BootStep::Bl => {
                dev.enter_bootloader()?;
            }
            BootStep::Hs => {
                let mut bl = dev.bootloader(9600, true)?;
                bl.handshake()?;
            }
            BootStep::BrL1 => {
                let mut bl = dev.bootloader(9600, true)?;
                bl.set_baudrate(true)?;
            }
            BootStep::BrL2 => {
                let mut bl = dev.bootloader(9600, true)?;
                bl.set_baudrate(false)?;
            }
            BootStep::RamCodeInfo => {
                let mut bl = dev.bootloader(9600, true)?;
                bl.ram_code_info()?;
            }
            BootStep::RamCode => {
                let mut bl = dev.bootloader(9600, true)?;
                bl.send_ram_code()?;
            }
            BootStep::PmInfo { firmware } => {
                let fw = FW::load_from_ihex(&firmware)?;
                let mut bl = dev.bootloader(9600, true)?;
                bl.send_fw_info(&fw, ClearMode::ClearSectors)?;
            }
            BootStep::Pm { firmware } => {
                let fw = FW::load_from_ihex(&firmware)?;
                let mut bl = dev.bootloader(9600, true)?;
                bl.load_fw(&fw, |position, total| {
                    println!("{} / {}", position, total);
                })?;
            }
            BootStep::Vf { firmware } => {
                let fw = FW::load_from_ihex(&firmware)?;
                let mut bl = dev.bootloader(9600, true)?;
                bl.verify_fw(&fw)?;
            }
            BootStep::Reset => {
                let mut bl = dev.bootloader(9600, true)?;
                bl.reset()?;
            }
        },
    };
    Ok(())
}

fn handle_mem_subcmd(device: &mut dyn Device, cmd: MemCmd) -> Result<()> {
    if let Some(fw) = cmd.compare {
        let fw = FW::load_from_ihex(&fw)?;
        let fw_size = fw.data.len();
        let buffer = read_mem_block(device, fw.load_address as usize, fw_size)?;
        for i in 0..fw_size as usize {
            if buffer[i] != fw.data[i] {
                println!(
                    "{:08X}: {:02X} - {:02X}",
                    fw.load_address + i as u32,
                    buffer[i],
                    fw.data[i]
                );
                break;
            }
        }
    } else {
        if cmd.start_address < 0x08000000u32 {
            bail!("Invalid start address should be larger than 0x8000000");
        }
        println!("start address: {:x}", cmd.start_address);

        if let Some(value) = cmd.write {
            write_mem_u32(&mut *device, cmd.start_address as usize, value)?;
        } else {
            let buffer =
                read_mem_block(&mut *device, cmd.start_address as usize, cmd.num as usize)?;
            if let Some(filename) = cmd.output_file {
                write_to_file(&filename, &buffer)?;
            }
            hexdump(&buffer);
        }
    };
    Ok(())
}

fn read_mem_block(device: &mut dyn Device, start_address: usize, num: usize) -> Result<Vec<u8>> {
    {
        println!("{} Entering bootloader", style("[1/3]").bold().dim(),);
        device.enter_bootloader().unwrap();
    }

    println!(
        "{} Setting baud rate {}...",
        style("[2/3]").bold().dim(),
        115200
    );
    let mut mmbl = device.bootloader(115200, false)?;
    mmbl.initialize()?;

    println!(
        "{} Reading flash {} bytes...",
        style("[3/3]").bold().dim(),
        num
    );
    Ok(mmbl.read_memory_block(start_address, num)?)
}

fn write_mem_u32(device: &mut dyn Device, start_address: usize, value: u32) -> Result<()> {
    {
        println!("{} Entering bootloader", style("[1/3]").bold().dim(),);
        device.enter_bootloader().unwrap();
    }

    println!(
        "{} Setting baud rate {}...",
        style("[2/3]").bold().dim(),
        115200
    );
    let mut mmbl = device.bootloader(115200, true)?;
    mmbl.initialize()?;

    println!("{} Writing flash ...", style("[3/3]").bold().dim());
    mmbl.write_memory_u32(start_address, value)?;
    Ok(())
}

fn handle_fw_subcmd(device: &mut dyn Device, firmware: &str, clear_chip: bool) -> Result<()> {
    let fw = FW::load_from_ihex(firmware)?;

    {
        print!("{} Get firmware version: ", style("[1/7]").bold().dim(),);
        let version = device.get_fw_version()?;
        println!("{}", version);

        println!("{} Entering bootloader", style("[2/7]").bold().dim(),);
        device.enter_bootloader().unwrap();

        println!(
            "{} Setting baud rate {}...",
            style("[3/7]").bold().dim(),
            115200
        );

        let mut mmbl = device.bootloader(115200, false)?;
        mmbl.initialize()?;

        println!("{} Downloading firmware...", style("[4/7]").bold().dim(),);
        let mode = if clear_chip {
            ClearMode::ClearChip
        } else {
            ClearMode::ClearSectors
        };
        let mut progress: Option<ProgressBar> = None;
        mmbl.download_fw(&fw, mode, |position, total| {
            if progress.is_none() {
                let pb = ProgressBar::new(total as u64);
                pb.set_style(ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({eta})")
                .unwrap()
                .progress_chars("#>-"));
                progress = Some(pb);
            }

            if let Some(pb) = &mut progress {
                pb.set_position(position as u64);
            }

        })?;
        println!("{} Starting firmware...", style("[6/7]").bold().dim(),);
        mmbl.reset()?;
    }

    thread::sleep(Duration::from_millis(300));
    let version = device.get_fw_version()?;
    println!(
        "{} After flashing firmware: {}",
        style("[7/7]").bold().dim(),
        version
    );

    Ok(())
}
