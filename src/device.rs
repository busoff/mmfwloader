use anyhow::{bail, Result};
use crc16::CCITT_FALSE;
use serialport::SerialPort;
use serialport::{self};

use crate::mmloader::Chip;
use crate::MMBootLoader;

pub struct LightEngine {
    pub port: String,
    pub chip: Chip,
}

pub trait Device {
    fn bootloader(&mut self, baudrate: u32, verbose: bool) -> Result<MMBootLoader>;
    fn enter_bootloader(&mut self) -> Result<()>;
    fn get_fw_version(&mut self) -> Result<String>;
}

impl LightEngine {
    pub fn new(port: &str, chip: Chip) -> Result<Self> {
        Ok(Self {
            port: port.to_string(),
            chip,
        })
    }
    fn read(&mut self, ser: &mut dyn SerialPort, address: u16) -> Result<u32> {
        let mut data = vec![0u8; 9];
        data[0] = 0x5A;
        data[1] = 0xA5;
        data[2] = 0x7E; // read command
        data[3] = address as u8;
        data[4] = (address >> 8) as u8;
        let crc = crc16::State::<CCITT_FALSE>::calculate(&data[2..5]);
        data[5] = (crc & 0xff) as u8;
        data[6] = (crc >> 8) as u8;
        data[7] = 0x55;
        data[8] = 0xAA;
        ser.write(&data)?;
        let mut buf = vec![0u8; 13];
        ser.read_exact(&mut buf)?;
        if buf[0] != 0x5A || buf[1] != 0xA5 || buf[11] != 0x55 || buf[12] != 0xAA {
            bail!(format!(
                "Invalid response: {}",
                buf.iter()
                    .map(|b| format!("{:02X}", b))
                    .collect::<Vec<_>>()
                    .join(" ")
            ));
        }
        let crc = crc16::State::<CCITT_FALSE>::calculate(&buf[2..9]);
        let actual = u16::from_le_bytes([buf[9], buf[10]]);
        if crc != actual {
            bail!(format!("CRC error {:x} - {:x}", crc, actual));
        }
        let data = u32::from_le_bytes(buf[5..9].try_into().unwrap());
        Ok(data)
    }
}

impl Device for LightEngine {
    fn bootloader(&mut self, baudrate: u32, verbose: bool) -> Result<MMBootLoader> {
        MMBootLoader::new(&self.port.clone(), baudrate, &self.chip, verbose)
    }

    fn enter_bootloader(&mut self) -> Result<()> {
        let mut ser = serialport::new(self.port.to_string(), 115200)
            .timeout(std::time::Duration::from_millis(1000))
            .open()?;

        // send upgrade fw command to light-engine
        let mut data = vec![0u8; 9];
        data[0] = 0x5A;
        data[1] = 0xA5;
        data[2] = 0x23; // read command
        data[3] = 0;
        data[4] = 0;
        let crc = crc16::State::<CCITT_FALSE>::calculate(&data[2..5]);
        data[5] = (crc & 0xff) as u8;
        data[6] = (crc >> 8) as u8;
        data[7] = 0x55;
        data[8] = 0xAA;

        ser.write(&data)?;
        println!(
            "bootloader: {:?}",
            data.iter()
                .map(|x| format!("{:02x}", x))
                .collect::<Vec<String>>()
                .join(" ")
        );
        Ok(())
    }

    fn get_fw_version(&mut self) -> Result<String> {
        let mut ser = serialport::new(self.port.to_string(), 115200)
            .timeout(std::time::Duration::from_millis(1000))
            .open()?;

        let mut ver = Vec::new();
        for i in 0..4 {
            let value = self.read(&mut *ser, 0x10 + i)?.to_le_bytes();
            ver.extend_from_slice(&value);
        }
        Ok(String::from_utf8_lossy(&ver).to_string())
    }
}

pub struct LEDDriver {
    pub port: String,
    pub chip: Chip,
}

impl LEDDriver {
    pub fn new(port: &str, chip: Chip) -> Result<Self> {
        Ok(Self {
            port: port.to_string(),
            chip,
        })
    }
    fn read_frame(&mut self, ser: &mut dyn SerialPort) -> Result<Vec<u8>> {
        let header = Self::read_exact(&mut *ser, 4)?;
        if header != [0x5a, 0xa5, 0x0f, 0xff] {
            bail!("Invalid header: {}", hex::encode(header));
        }
        let mut frame_buf: Vec<u8> = Vec::new();
        frame_buf.extend_from_slice(&header);
        while frame_buf[frame_buf.len() - 2..] != [0x55, 0xaa] {
            frame_buf.extend(Self::read_exact(&mut *ser, 1)?);
        }
        Ok(frame_buf)
    }

    fn read_exact(ser: &mut dyn SerialPort, n: usize) -> Result<Vec<u8>> {
        let mut reads = 0 as usize;
        let mut buf = vec![0u8; n];
        while reads < buf.len() {
            let n = ser.read(&mut buf[reads..])?;
            reads += n;
        }
        Ok(buf)
    }
}

impl Device for LEDDriver {
    fn bootloader(&mut self, baudrate: u32, verbose: bool) -> Result<MMBootLoader> {
        MMBootLoader::new(&self.port.clone(), baudrate, &self.chip, verbose)
    }

    fn enter_bootloader(&mut self) -> Result<()> {
        let mut ser = serialport::new(self.port.to_string(), 19200)
            .timeout(std::time::Duration::from_millis(1000))
            .open()?;

        // set up serial port for ERP232 protocol
        let reset_cmd = [0x5a, 0xa5, 0xff, 0xf0, 0x23, 0xf5, 0xf1, 0x55, 0xaa];
        ser.write_all(&reset_cmd)?;

        Ok(())
    }

    fn get_fw_version(&mut self) -> Result<String> {
        let mut ser = serialport::new(self.port.to_string(), 19200)
            .timeout(std::time::Duration::from_millis(1000))
            .open()?;
        let cmd = vec![0x5a, 0xa5, 0xff, 0xf0, 0x01, 0xf1, 0xd1, 0x55, 0xaa];
        ser.write_all(&cmd)?;
        let frame = self.read_frame(&mut *ser)?;
        let version = frame[5..frame.len() - 5]
            .iter()
            .map(|x| if *x == 0u8 { ' ' } else { *x as char })
            .collect::<String>();
        Ok(version)
    }
}
