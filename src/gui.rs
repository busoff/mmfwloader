#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use std::env;
use std::sync::mpsc::{channel, Receiver};
use std::thread::JoinHandle;

use anyhow::Result;
use eframe::egui::{self, Label, RichText, WidgetText};

use mmloader::{Chip, Device, LEDDriver, LightEngine};
use mmloader::{ClearMode, FW};

struct GUI {
    port: String,
    chip: Chip,
    port_opened: bool,
    version_text: String,
    version_error: String,
    fw_path: Option<String>,
    handle: Option<JoinHandle<()>>,
    flash_status_receiver: Option<Receiver<DownloadStatus>>,
    flash_progress: f32,
    flash_started: bool,
    clear_chip: bool,
    modal_opened: bool,
    modal_message: String,
}

impl Default for GUI {
    fn default() -> Self {
        Self {
            port: Default::default(),
            chip: Chip::MM32F0020,
            port_opened: Default::default(),
            version_text: Default::default(),
            version_error: Default::default(),
            fw_path: Default::default(),
            handle: Default::default(),
            flash_status_receiver: Default::default(),
            flash_progress: Default::default(),
            flash_started: Default::default(),
            clear_chip: Default::default(),
            modal_opened: Default::default(),
            modal_message: Default::default(),
        }
    }
}

impl eframe::App for GUI {
    fn update(&mut self, ctx: &egui::Context, _: &mut eframe::Frame) {
        // show a modal dialog
        if self.modal_opened {
            egui::Window::new("Error").show(ctx, |ui| {
                ui.vertical(|ui| {
                    ui.label(RichText::new(&self.modal_message).color(ui.visuals().error_fg_color));
                    ui.add_space(30.0);
                    if ui.button("Close").clicked() {
                        self.modal_opened = false;
                    }
                })
            });
        }
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.set_enabled(!self.modal_opened);
            ui.horizontal(|ui| {
                ui.label("Port: ");

                egui::ComboBox::from_label("")
                    .selected_text(format!("{}", self.port))
                    .show_ui(ui, |ui| {
                        let ports = serialport::available_ports().unwrap();
                        for port in ports {
                            match port.port_type {
                                serialport::SerialPortType::Unknown => continue,
                                _ => {}
                            }
                            if ui
                                .selectable_value(
                                    &mut self.port,
                                    port.port_name.clone(),
                                    port.port_name.clone(),
                                )
                                .clicked()
                            {
                                println!("Port changed to: {}", self.port);
                                self.port_opened = false;
                            };
                        }
                    });
                let chip: String = self.chip.into();
                egui::ComboBox::from_label("Select chip")
                    .selected_text(chip)
                    .show_ui(ui, |ui| {
                        if ui
                            .selectable_value(&mut self.chip, Chip::MM32F0020, "MM32F0020")
                            .clicked()
                        {
                            self.port_opened = false;
                        }
                        if ui
                            .selectable_value(&mut self.chip, Chip::MM32F0140, "MM32F0140")
                            .clicked()
                        {
                            self.port_opened = false;
                        }
                    });

                if !self.port_opened && self.port.len() > 0 {
                    println!("open port: {}: chip:{:?}", self.port, self.chip);
                    let device: Result<Box<dyn Device>> = match self.chip {
                        Chip::MM32F0020 => LEDDriver::new(&self.port, Chip::MM32F0020)
                            .map(|d| Box::new(d) as Box<dyn Device>),
                        Chip::MM32F0140 => LightEngine::new(&self.port, Chip::MM32F0140)
                            .map(|d| Box::new(d) as Box<dyn Device>),
                    };
                    if let Ok(mut dev) = device {
                        if let Ok(version) = dev.get_fw_version() {
                            self.version_text = version.split(" ").collect::<Vec<&str>>()[0].into();
                            self.version_error = "".to_owned();
                            self.port_opened = true;
                        } else {
                            self.version_error = "Failed to get version".to_owned();
                            self.port_opened = true;
                        }
                    } else {
                        self.version_error = "Failed to get version".to_owned();
                        self.port_opened = true;
                    }
                }
                if ui.button("Test port").clicked() {
                    // get version
                    let device: Result<Box<dyn Device>> = match self.chip {
                        Chip::MM32F0020 => LEDDriver::new(&self.port, Chip::MM32F0020)
                            .map(|d| Box::new(d) as Box<dyn Device>),
                        Chip::MM32F0140 => LightEngine::new(&self.port, Chip::MM32F0140)
                            .map(|d| Box::new(d) as Box<dyn Device>),
                    };
                    if let Ok(mut dev) = device {
                        if let Ok(version) = dev.get_fw_version() {
                            self.version_text = version.split(" ").collect::<Vec<&str>>()[0].into();
                            self.version_error = "".to_owned();
                        } else {
                            self.version_error = "Failed to get version".to_owned();
                        }
                    } else {
                        self.version_error = "Failed to get version".to_owned();
                    }
                }
                if self.version_error.len() > 0 {
                    ui.add(Label::new(
                        RichText::new(&self.version_error).color(ui.visuals().error_fg_color),
                    ));
                } else {
                    ui.add(Label::new(RichText::new(&self.version_text)));
                }
            });

            ui.add_space(10.0);

            ui.horizontal(|ui| {
                if ui.button("Open file…").clicked() {
                    if let Some(path) = rfd::FileDialog::new()
                        .add_filter("Firmware", &["hex"])
                        .pick_file()
                    {
                        self.fw_path = Some(path.display().to_string());
                    }
                }

                if let Some(picked_path) = &self.fw_path {
                    ui.horizontal(|ui| {
                        ui.label("file:");
                        ui.monospace(picked_path);
                    });
                }
            });
            ui.add_space(10.0);
            ui.horizontal(|ui| {
                if ui.button("Program").clicked() {
                    if self.flash_status_receiver.is_none() {
                        self.download_fw();
                    }
                }
                ui.checkbox(&mut self.clear_chip, "Clear chip");

                if let Some(status_rx) = &mut self.flash_status_receiver {
                    if let Ok(status) = status_rx.try_recv() {
                        match status {
                            DownloadStatus::Downloading(position, total) => {
                                self.flash_progress = position / total;
                                self.flash_started = true;
                            }
                            DownloadStatus::Done => {
                                self.flash_status_receiver = None;
                                self.flash_progress = 1.0;
                                self.flash_started = false;
                                self.handle = None;
                                self.port_opened = false; // trigger to get new version
                                                          // wait for background thread to finish
                                if let Some(handle) = self.handle.take() {
                                    handle.join().unwrap();
                                }
                            }
                            DownloadStatus::Error(e) => {
                                self.flash_status_receiver = None;
                                self.flash_progress = 0.0;
                                self.flash_started = false;
                                // wait for background thread to exit
                                if let Some(handle) = self.handle.take() {
                                    handle.join().unwrap();
                                }
                                self.version_error = e;
                            }
                        }
                    }
                }

                ui.add(
                    egui::ProgressBar::new(self.flash_progress)
                        .show_percentage()
                        .animate(self.flash_started),
                );
            });
        });
    }
}

enum DownloadStatus {
    Downloading(f32, f32),
    Done,
    Error(String),
}
impl GUI {
    fn download_fw(&mut self) {
        if self.fw_path.is_none() {
            self.modal_opened = true;
            self.modal_message = "Please select a firmware file.".to_owned();
            return;
        }
        if self.port.len() == 0 {
            self.modal_opened = true;
            self.modal_message = "Please select a port.".to_owned();
            return;
        }
        self.flash_started = true;
        self.flash_progress = 0.0;
        let (req_tx, req_rx) = channel();
        let port = self.port.clone();
        let path = self.fw_path.clone().unwrap();
        let clear_chip = self.clear_chip;
        let chip = self.chip.clone();
        self.handle = Some(std::thread::spawn(move || {
            let status_tx = req_tx;
            if let Err(e) = Self::flash_fw(&port, chip, &path, clear_chip, |position, total| {
                status_tx
                    .send(DownloadStatus::Downloading(position as f32, total as f32))
                    .unwrap();
            }) {
                status_tx
                    .send(DownloadStatus::Error(e.to_string()))
                    .unwrap();
            } else {
                status_tx.send(DownloadStatus::Done).unwrap();
            }
        }));
        self.flash_status_receiver = Some(req_rx);
    }

    fn flash_fw<F>(port: &str, chip: Chip, path: &str, clear_chip: bool, update: F) -> Result<()>
    where
        F: FnMut(f32, f32) -> (),
    {
        println!("Flashing firmware: {} to port: {}", path, port);
        let fw = FW::load_from_ihex(path)?;

        let device: Result<Box<dyn Device>> = match chip {
            Chip::MM32F0020 => {
                LEDDriver::new(&port, Chip::MM32F0020).map(|d| Box::new(d) as Box<dyn Device>)
            }
            Chip::MM32F0140 => {
                LightEngine::new(&port, Chip::MM32F0140).map(|d| Box::new(d) as Box<dyn Device>)
            }
        };
        let mut dev = device?;
        println!("reset bootloader");
        dev.enter_bootloader()?;
        let mut mmbl = dev.bootloader(115200, false)?;
        mmbl.initialize()?;
        println!("download fw");
        let mode = if clear_chip {
            ClearMode::ClearChip
        } else {
            ClearMode::ClearSectors
        };
        mmbl.download_fw(&fw, mode, update)?;
        println!("reset MCU");
        mmbl.reset()?;
        Ok(())
    }
}

fn main() -> Result<(), eframe::Error> {
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).

    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_inner_size([680.0, 140.0]),
        ..Default::default()
    };

    eframe::run_native(
        &format!("MindMotion Loader GUI {}", env!("CARGO_PKG_VERSION")),
        options,
        Box::new(|_cc| Box::<GUI>::default()),
    )
}
